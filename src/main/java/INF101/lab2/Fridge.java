package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge  implements IFridge {
    int max_size = 20;
    ArrayList<FridgeItem> fridge = new ArrayList <>();
    
    public int totalSize () {
        return max_size;
    }
    @Override
    public int nItemsInFridge() {
        return fridge.size();
        // TODO Auto-generated method stub
        //return 0;
    }
    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < max_size) {
            fridge.add (item);
            return true;
        }

        else {
            return false;
        
        }
        
    }
    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge() > 0) {
            fridge.remove (item);
        }
        else {
            throw new NoSuchElementException();
        
        }
        // TODO Auto-generated method stub
        
    }
    @Override
    public void emptyFridge() {
        while(fridge.size() > 0 ) {
            fridge.clear();
        }
        // TODO Auto-generated method stub
        
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> deletedItems = new ArrayList<>();
        
        for (FridgeItem i : fridge) {
            if (i.hasExpired()) {
                deletedItems.add(i);
                
            }
        }
        for (FridgeItem n : deletedItems) {
            fridge.remove(n);
        }
        
        return deletedItems;
    }
    
}
